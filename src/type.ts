export type Ticket = {
  price: number,
  carrier: string,
  segments:
  [{
    origin: string,
    destination: string,
    date: string,
    stops: string[],
    duration: number
  },
    {
      origin: string,
      destination: string,
      date: string,
      stops: string[],
      duration: number
    }]
}



export type TariffType = [{name: "cheapest", isActive: boolean }, {name: "fastest", isActive: boolean}, {name: "optimal" , isActive: boolean}];

export type StopsType = [
  {
    name: 'all',
    isActive: boolean
  },
  {
    name: 0,
    isActive: boolean
  },
  {
    name: 1,
    isActive: boolean
  },
  {
    name: 2,
    isActive: boolean
  },
  {
    name: 3,
    isActive: boolean
  },
];

