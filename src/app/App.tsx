import React, { useEffect, useMemo, useState } from 'react';
import styles from './App.module.css';
import logo from '../icon/logo.png';
import { StopsFilter } from '../stopsFilter/stopsFilter';
import { TicketBlock } from '../ticketBlock/ticketBlock';
import { getSearchId, getTickets } from '../api/api';
import { TariffType, Ticket, StopsType } from '../type';
import { TariffsFilter } from '../tariffsFilter/tariffsFilter';

function App() {
  const [ticketsPackage, setTicketsPackage] = useState<Ticket[]>([]);
  const [cheapestTickets, setCheapestTickets] = useState<Ticket[]>([]);
  const [fastestTickets, setFastestTickets] = useState<Ticket[]>([]);
  const [optimalTickets, setOptimalTickets] = useState<Ticket[]>([]);
  const [activeTariffType, setActiveTariffType] = useState<TariffType>([{name: "cheapest", isActive: true },{name: "fastest", isActive: false}, {name: "optimal" , isActive: false}, ]);
  const [stopsActivityList, setStopsActivityList] = useState<StopsType>([{ name: 'all', isActive: true }, { name: 0, isActive: false }, { name: 1, isActive: false }, { name: 2, isActive: false }, { name: 3, isActive: false }]);

  let activeTariff: Ticket[] = useMemo(() => {
    for(let type of activeTariffType){
      
      if(type.isActive && type.name === "cheapest"){
        return cheapestTickets;
      }

      if(type.isActive && type.name === "fastest"){
        return fastestTickets;
      }

      if(type.isActive && type.name === "optimal"){
        return optimalTickets;
      }
    }

    return []
  }, [activeTariffType, cheapestTickets, fastestTickets, optimalTickets])

  useEffect(() => {
    getSearchId()
      // .then(searchId => {
      //   const timerId = setInterval(() => {
      //     getTickets(searchId)
      //       .then(ticketsQuery => {
      //         const stop: boolean = ticketsQuery.stop;

      //         if (stop) {
      //           alert('готово')
      //           clearInterval(timerId);
      //         }

      //         setTicketsPackage((oldState) => {
      //           const newState = [...oldState, ...ticketsQuery.tickets];
      //           return newState;
      //         })
      //       })
      //       .catch(error => console.log('Ошибочка запросика'))
      //   }, 3000)
      // })
      .then(searchId => {
        getTickets(searchId)
          .then(ticketsQuery => {
            setTicketsPackage((oldState) => {
              const newState = [...oldState, ...ticketsQuery.tickets];
              return newState;
            })
          })
      })
  }, []);

  useEffect(() => {
    const cheapestTickets = [...ticketsPackage].sort((a, b) => a.price - b.price);

    const fastestTicket = [...ticketsPackage].sort((a, b) => {
      const A = (a.segments[0].duration + a.segments[1].duration);
      const B = (b.segments[0].duration + b.segments[1].duration);

      if (A < B) {
        return -1
      }
      
      if (A > B) {
        return 1
      
      }
      return 0
    });

    const optimalTickets = [...ticketsPackage].sort((a, b) => {
      const segmentsA = (a.segments[0].duration + a.segments[1].duration);
      const segmentsB = (b.segments[0].duration + b.segments[1].duration);
      const A = a.price / segmentsA;
      const B = b.price / segmentsB;
      
      if (A < B) {
        return -1
      
      }
      if (A > B) {
        return 1
      
      }
      return 0
    });

    setCheapestTickets(cheapestTickets);
    setFastestTickets(fastestTicket);
    setOptimalTickets(optimalTickets);
  }, [ticketsPackage]);


  function filterActiveStops(ticket: Ticket) {
    const stopsCount = ticket.segments[0].stops.length + ticket.segments[1].stops.length;

    for (let transfer of stopsActivityList) {
      
      if (transfer.name === "all" && transfer.isActive === true) {
        return true
      
      } else {
        if(stopsCount === transfer.name && transfer.isActive === true){
          return true
        }
      }
    }
  }

  function handlerStopsActivityList(isActive: boolean, index: number) {
    setStopsActivityList(oldState => {
      const newState: StopsType = [...oldState];   
      
      if (index > 0) {
        newState[0].isActive = false;
        newState[index].isActive = !isActive;
      } else {
        newState.forEach((transfer, index) => {
          if (index === 0) {
            transfer.isActive = true;
          } else {
            transfer.isActive = false;
          }
        })
      }
      return newState;
    });
  }

  function handlerActiveTariffType(tariffType: string){
    setActiveTariffType( oldState => {
      const newState: TariffType = [...oldState];
      for(let type of newState){
        if(type.name === tariffType){
          type.isActive = true;
        } else {
          type.isActive = false;
        }
      }
      return newState;
    })
  }


  return (
    <div className={styles.app}>

      <div className={styles.iconBlock}>
        <img src={logo} alt='Logo' />
      </div>

      <div className={styles.baseBlock}>
        <StopsFilter onChange={handlerStopsActivityList} stopsActivityList={stopsActivityList} />

        <div style={{ width: '504px' }}>
          <TariffsFilter onClick={handlerActiveTariffType} activeTariffType={activeTariffType} />
          <TicketBlock activeTariff={activeTariff} filterActiveStops={filterActiveStops} />
        </div>

      </div>
    </div>
  );
}


export default App;
