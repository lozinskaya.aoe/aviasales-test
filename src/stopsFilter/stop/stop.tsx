import React from 'react';
import {StopsType } from '../../type';

type Props = { onChange: (isActive: boolean, index:number) => void, name: string | number, isActive: boolean, index: number }

export function Stop(props: Props) {
    const name = props.name;
    const isActive = props.isActive;
    const index = props.index;
    const text = addText(name);

    return (
        <label onChange={() => props.onChange(isActive, index)}>
            <input type='checkbox' name={`${name}`} checked={isActive}/>
            <span>{text}</span>
        </label>
    )
}

function addText(name: string | number) {
    switch (name) {
        case "all": return "Все";
        case 0: return "Без пересадок";
        case 1: return "1 пересадка";
        default: return `${name} пересадок`
    }
}