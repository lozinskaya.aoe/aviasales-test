import React from 'react';
import { StopsType } from '../type';
import { Stop } from './stop/stop';
import styles from './stopsFilter.module.css'

type Props = { onChange: (isActive: boolean, index: number) => void, stopsActivityList: StopsType };

export function StopsFilter(props: Props) {
    return (
        <div className={styles.stopFilter} >
            <h1>Количество пересадок</h1>
            {props.stopsActivityList.map((stop, index) => <Stop key={index} index={index} onChange={props.onChange} name={stop.name} isActive={stop.isActive} />)}
        </div>
    )
}

