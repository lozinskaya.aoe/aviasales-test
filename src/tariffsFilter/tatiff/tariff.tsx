import React from 'react';
import styles from './tariff.module.css'


type Props = { onClick: (tariffType: string) => void, name: string, isActive: boolean} 

export  function Tariff(props: Props){
    const name = props.name;
    const isActive = props.isActive;
    const text = addText(name);
    const style = addStyle(name);


    return (
        <div
            className={isActive? `${style} ${styles.active}` : style}
            onClick={() => props.onClick(name)}>
            {text}
        </div>
    )
}

function addText(name: string) {
    switch (name) {
        case "cheapest": return "Cамый дешевый";
        case "fastest": return "Самый быстрый";
        default: return "Оптимальный";
    }
}

function addStyle(name: string) {
    switch (name) {
        case "cheapest": return styles.cheapest;
        case "fastest": return styles.fastest;
        default: return styles.optimal;
    }
}