import React from 'react';
import { TariffType } from '../type';
import styles from './tariffsFilter.module.css'
import { Tariff } from './tatiff/tariff';

type Props = { onClick: (tariffType: string) => void, activeTariffType: TariffType };

export function TariffsFilter(props: Props) {
    const activeTariffType = props.activeTariffType;

    return (<div className={styles.tariffsFilter}>
        {activeTariffType.map((tarif, index) => <Tariff key={index} name={tarif.name} isActive={tarif.isActive} onClick={props.onClick}/>)}
    </div>)
}


