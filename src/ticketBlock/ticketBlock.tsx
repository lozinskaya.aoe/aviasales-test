import React, { useState } from 'react';
import { AirTicket } from './airTicket/airTicket';
import styles from './ticketBlock.module.css';
import { Ticket } from '../type';
import { ShowTicketButtom } from './showTicketButtom/showTicketButtom';

type Props = { activeTariff: Ticket[], filterActiveStops: (ticket: Ticket) => void};

export function TicketBlock(props: Props) {
    const [countFilter, setCountFilter] = useState<number>(5);

    function showTicket() {
        setCountFilter((oldState: number) => {
            const newState = oldState + 5;
            return newState;
        })
    }

    return (
        <div className={styles.ticketBlock}>

            {props.activeTariff.filter(ticket => props.filterActiveStops(ticket)).map((ticket, index) => {
                if (index < countFilter) {
                    return <AirTicket key={index} ticketInformation={ticket} />
                }
            })}

            <ShowTicketButtom onClick={showTicket}/>

        </div>)
}