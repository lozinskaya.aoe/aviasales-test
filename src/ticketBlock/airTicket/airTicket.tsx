import React from 'react';
import styles from './airTicket.module.css';
import { Ticket } from '../../type';


type Props = { ticketInformation: Ticket }
export function AirTicket(props: Props) {
    const icon = 'https://pics.avs.io/99/36/' + props.ticketInformation.carrier + '.png';
    const price = `${retouchPrice(props.ticketInformation.price)} P`;


    return (
        <div className={styles.ticket}>

            <div className={styles.header}>
                {price}
                <img src={icon} className={styles.icon} />
            </div>

            {props.ticketInformation.segments.map(item => {
                const duration = item.duration;
                const origin = item.origin;
                const destination = item.destination;
                const stopsCount = item.stops.length ? `${item.stops.length} ПЕРЕСАДОК` : `БЕЗ ПЕРЕСАДОК`;
                const stops = item.stops.join(', ');
                const dateThere = new Date(item.date);
                const timeOfDepartureThere = dateThere.toLocaleTimeString(navigator.language, { hour: '2-digit', minute: '2-digit' }).replace(/(:\d{2}| [AP]M)$/, "");
                const timeOfArrivalThere = new Date(dateThere.setMinutes(duration)).toLocaleTimeString(navigator.language, { hour: '2-digit', minute: '2-digit' }).replace(/(:\d{2}| [AP]M)$/, "");
                const onTheWayThere = convectorInHour(duration);

                return (
                    <div className={styles.way}>

                        <div className={styles.column}>
                            <div className={styles.title}>{origin} - {destination}</div>
                            <div>{timeOfDepartureThere} - {timeOfArrivalThere} </div>
                        </div>

                        <div className={styles.column}>
                            <div className={styles.title}>В пути</div>
                            <div>{onTheWayThere}</div>
                        </div>

                        <div className={styles.column}>
                            <div className={styles.title}>{stopsCount}</div>
                            <div>{stops}</div>
                        </div>

                    </div>
                );
            })}
        </div>
    )
}

function convectorInHour(minutes: number) {
    const hour = Math.trunc(minutes / 60);
    let minute = (minutes % 60).toString();

    if (minute.length === 0) {
        return `${hour}ч 00м`;
    }
    
    if (minute.length === 1) {
        return `${hour}ч 0${minute}м`;
    }

    return `${hour}ч ${minute}м`;
}

function retouchPrice(price: number) {
    const oldPrice = String(price).split('').reverse();
    const retouchedPrice = oldPrice.map((item, index) => {
        if ((index + 1) % 3 === 0) {
            return ` ${item}`;
        }

        return item;
    });
    
    return retouchedPrice.reverse().join('');
}