import React from 'react';
import styles from './showTicketButtom.module.css'

type Props = {onClick: () => void};

export function ShowTicketButtom(props: Props){

    return(
        <button className={styles.button} onClick={() => props.onClick()}>Показать еще 5 билетов!</button>
    );
}