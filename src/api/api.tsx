const searchIcon = 'https://pics.avs.io/99/36/';
const searchId = 'https://front-test.beta.aviasales.ru/search';
const tickets  = 'https://front-test.beta.aviasales.ru/tickets?searchId=';



export function getSearchId() {
    const promise = fetch(searchId).then(response => response.json()).then(response => response.searchId);
    return promise;
}

export function getTickets(searchId:string) {
    const promise = fetch(tickets + searchId).then(response => response.json());
    return promise;
}


export function updateSearchId(id:string) {
    const promise = fetch(tickets + id).then(response => response.json());
    return promise;
}
